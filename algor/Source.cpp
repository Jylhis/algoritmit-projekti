#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <random>
#include <fstream>
#include <iterator>

const time_t waitTime = time(NULL) + 10;
// Max size: 4294967295
const unsigned long int arraySize = 15000;

inline void output(std::vector<int>* vec, const std::string file)
{
  std::ofstream out_file(file);
  std::ostream_iterator<int> output_iterator(out_file, "\n");
  std::copy(vec->begin(), vec->end(), output_iterator);
  out_file.close();
}

void bubblesort(std::vector<int>* list) {
  constexpr auto arrSize = arraySize-1;
  for (size_t i = 0; i < arrSize; ++i) {
    for (size_t j = 0; j < arrSize - 1 - i; ++j) {
      if ((*list)[j] > (*list)[j + 1]) {
        std::swap((*list)[j], (*list)[j + 1]);
      }
    }
  }
}

void mergesort(std::vector<int>* list)
{
  if (list->size() <= 1) {
    return;
  }

  std::size_t middle = list->size() / 2;
  std::vector<int> left(list->begin(), list->begin() + middle);
  std::vector<int> right(list->begin() + middle, list->end());

  mergesort(&left);
  mergesort(&right);

  std::merge(left.begin(), left.end(), right.begin(), right.end(), list->begin());
}

int main(int argc, char *argv[])
{
  std::vector<int> list;
  list.reserve(arraySize);
  std::vector<int> list2;
  list2.reserve(arraySize);

  // Generate list
  std::default_random_engine generator;
  std::uniform_int_distribution<int> distribution(0, 9999); // Number between 1..9999
  for (size_t i = 0; i < arraySize; i++) {
    auto num = distribution(generator);
    list.emplace_back(num);
    list2.emplace_back(num);
  }

  output(&list, "original.out");

  std::cout << "Start bubblesort" << std::endl;
  clock_t time = clock();
  bubblesort(&list);
  time = clock() - time;
  std::cout << "Bubblesort time: " << ((float)time) / CLOCKS_PER_SEC << std::endl;

  output(&list, "bubble.out");


  std::cout << "Start mergesort" << std::endl;
  time = clock();
  mergesort(&list2);
  time = clock() - time;
  std::cout << "Mergesort time: " << ((float)time) / CLOCKS_PER_SEC << std::endl;

  output(&list2, "merge.out");

  return 0;
}
